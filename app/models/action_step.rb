class ActionStep
  include Neo4j::ActiveNode
  property :title, type: String
  property :description, type: String
  property :level, type: Integer
  has_many :in, :users, rel_class: ActionStatus, unique: true

  validates :level, inclusion: 1..9, allow_nil: true

  def status_for_user(user)
    return nil if user.nil?
    user.action_steps.first_rel_to self
  end

  def self.sample
    (1..9).map do |level|
      ActionStep.where(level: level).first
    end.compact!
  end
end
