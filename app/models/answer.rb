class Answer 
  include Neo4j::ActiveRel
  property :value, type: bool
  validates :value, presence: true

end
