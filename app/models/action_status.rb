class ActionStatus
  include Neo4j::ActiveRel

  before_save :timestamp

  from_class User
  to_class ActionStep
  type 'action_status'

  property :status, type: Integer
  property :created_at, type: Integer

  validates :status, presence: true

  ActionStatus.creates_unique_rel

  STATUS = {
    done: 1,
    todo: 2,
    later: 3
  }

  def timestamp
    self.created_at = Time.now.getutc if created_at.nil?
  end
end
