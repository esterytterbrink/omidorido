class Question 
  include Neo4j::ActiveNode
  property :title, type: String
  validates :title, presence: true
end
