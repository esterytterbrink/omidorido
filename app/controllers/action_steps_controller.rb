class ActionStepsController < ApplicationController
  before_action :set_step, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!,
                only: [:new, :edit, :create, :update, :destroy]

  # GET /steps
  # GET /steps.json
  def index
    @action_steps = ActionStep.all
  end

  # GET /steps/1
  # GET /steps/1.json
  def show
  end

  # GET /steps/new
  def new
    @action_step = ActionStep.new
  end

  # GET /steps/1/edit
  def edit
  end

  # POST /steps
  # POST /steps.json
  def create
    @action_step = ActionStep.new(step_params)
    respond_to do |format|
      if @action_step.save
        format.html do
          redirect_to @action_step, notice: 'Step was successfully created.'
        end
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /steps/1
  # PATCH/PUT /steps/1.json
  def update
    respond_to do |format|
      if @action_step.update(step_params)
        format.html do
          redirect_to @action_step,
                      notice: 'Step was successfully updated.'
        end
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /steps/1
  # DELETE /steps/1.json
  def destroy
    @action_step.destroy
    respond_to do |format|
      format.html do
        redirect_to steps_url,
                    notice: 'Step was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_step
    @action_step = ActionStep.find(params[:id])
  end

  # Never trust parameters from the scary internet,
  # only allow the white list through.
  def step_params
    params.require(:action_step).permit(:title, :description, :level)
  end
end
