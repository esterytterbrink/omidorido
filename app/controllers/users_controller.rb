class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:home]
  def home
    @todo = current_user.action_steps(:a, :r).rel_where(
        status: ActionStatus::STATUS[:todo]
    )
    @done = current_user.action_steps(:a, :r).rel_where(
        status: ActionStatus::STATUS[:done]
    )
    @later = current_user.action_steps(:a, :r).rel_where(
        status: ActionStatus::STATUS[:later]
    )
  end

  def browse_steps
    @steps = ActionStep.sample if current_user.nil?
    @steps = current_user.next_available_actions unless current_user.nil?
  end
end
