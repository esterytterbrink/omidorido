class ActionStatusController < ApplicationController
  before_action :authenticate_user!
  def create
    user = current_user
    action_step = ActionStep.find(params[:action_step])
    status = params[:status]
    ActionStatus.create(from_node: user, to_node: action_step, status: status)
    session[:return_to] ||= request.referer
    redirect_to session.delete(:return_to)
  end

  def update
    user = current_user
    status = params[:status]
    action_step = ActionStep.find(params[action_step])
    action_status = user.rel_where(action_step: action_step)
    action_status.status = status
    action_status.save!
  end
end
