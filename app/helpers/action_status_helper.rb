module ActionStatusHelper
  def change_status_link(status, step)
    return links_for_blank(step) if status.nil?
    return links_for_todo(step) if status.status == ActionStatus::STATUS[:todo]
    links_for_later(step) if status.status == ActionStatus::STATUS[:later]
  end

  def links_for_blank(step)
    content = content_tag(:div, change_to_todo_link(step))
    content += content_tag(:div, change_to_done_link(step))
    content + content_tag(:div, change_to_later_link(step))
  end

  def links_for_todo(step)
    content = content_tag(:div, change_to_done_link(step))
    content + content_tag(:div, change_to_later_link(step))
  end

  def links_for_later(step)
    content_tag :div, change_to_todo_link(step)
  end

  def change_to_done_link(step)
    link_to 'Klart!',
            action_status_index_path(
                action_step: step,
                status: ActionStatus::STATUS[:done]
            ),
            method: :post
  end

  def change_to_todo_link(step)
    link_to 'Bra ide!',
            action_status_index_path(
                action_step: step,
                status: ActionStatus::STATUS[:todo]
            ),
            method: :post
  end

  def change_to_later_link(step)
    link_to 'Senare',
            action_status_index_path(
                action_step: step,
                status: ActionStatus::STATUS[:later]
            ),
            method: :post
  end
end
