FactoryGirl.define do
  factory :action_step do
    title 'An action step title'
    description 'A slightly longer text describing what the action step is of'
  end
end
