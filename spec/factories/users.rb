FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "svea.svensson#{n}@example.com" }
    password '1234asdf'
  end
end
