require 'rails_helper'

describe User do
  it 'is valid with an email and a password ' do
    user = described_class.new(email: 'eva.e@example.com', password: 'abcdefgh')
    expect(user).to be_valid
  end

  it 'has a valid factory' do
    user = FactoryGirl.build(:user)
    expect(user).to be_valid
  end
end
