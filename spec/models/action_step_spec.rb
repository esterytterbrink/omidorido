require 'rails_helper'

describe ActionStep do
  it 'is valid with a title' do
    action_step = described_class.new(title: 'A title')
    expect(action_step).to be_valid
  end

  it 'has a valid factory' do
    expect(FactoryGirl.build(:action_step)).to be_valid
  end

  context 'scopes' do
    let(:step1) { FactoryGirl.create(:action_step, title: 'Action step 1') }
    let(:step2) { FactoryGirl.create(:action_step, title: 'Action step 2') }
    let(:step3) { FactoryGirl.create(:action_step, title: 'Action step 3') }
    let(:user) { FactoryGirl.create(:user) }

    it 'finds all steps that are done for a user' do
      ActionStatus.create(
          from_node: user,
          to_node: step1,
          status: ActionStatus::STATUS[:todo]
      )
      ActionStatus.create(
          from_node: user,
          to_node: step2,
          status: ActionStatus::STATUS[:done]
      )
      ActionStatus.create(
          from_node: user,
          to_node: step3,
          status: ActionStatus::STATUS[:done]
      )
      expect(user.action_steps).to match_array([step1, step2, step3])
      rel_array = user.action_steps(:a, :r).rel_where(
          status: ActionStatus::STATUS[:done]
      )
      expect(rel_array).to match_array([step2, step3])
    end
  end

  context 'sample steps' do
    let!(:step1) { create(:action_step, title: 'Action step 1 1', level: 1) }
    let!(:step3) { create(:action_step, title: 'Action step 3', level: 3) }
    let!(:step11) { create(:action_step, title: 'Action step 1 2', level: 1) }

    let(:sample) { described_class.sample }

    it 'finds two steps' do
      expect(sample.length).to eq(2)
    end

    it 'finds the only step 3' do
      expect(sample).to include(step3)
    end

    it 'finds one of the steps 1' do
      expect(sample.include?(step1) || sample.include?(step11)).to be_truthy
    end

    it 'does not find both of the step1' do
      expect(sample.include?(step1) && sample.include?(step11)).to be_falsey
    end
  end

  context '#status_for_user' do
    let(:user) { FactoryGirl.create(:user) }
    let(:step1) { FactoryGirl.create(:action_step, title: 'Action step 1') }

    it 'finds the relationship for a action step and a user' do
      a1 = ActionStatus.create(
          from_node: user,
          to_node: step1,
          status: ActionStatus::STATUS[:todo]
      )
      expect(step1.status_for_user user).to eq a1
    end
  end
end
