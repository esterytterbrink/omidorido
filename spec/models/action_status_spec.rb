require 'rails_helper'

describe ActionStatus do
  let(:user) { FactoryGirl.create(:user) }
  let(:action_step) { FactoryGirl.create(:action_step) }

  it 'is valid with action_step and user' do
    status = ActionStatus::STATUS[:done]
    action_status = described_class.create(
      from_node: user,
      to_node: action_step,
      status: status
    )
    expect(action_status).to be_valid
  end

  it 'only creates one action_status for each action_step - user pair' do
    done = ActionStatus::STATUS[:done]
    described_class.create(from_node: user, to_node: action_step, status: done)
    todo = ActionStatus::STATUS[:todo]
    described_class.create(from_node: user, to_node: action_step, status: todo)
    expect(action_step.users.rels_to(user).count).to eq 1
    expect(action_step.users.rels_to(user).first.status).to eq(todo)
  end
end
